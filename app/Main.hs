module Main where

import           UI.Console                     ( runGame )
import           AI.QLearning                   ( runLearning )

main :: IO ()
-- main = runGame
main = runLearning
