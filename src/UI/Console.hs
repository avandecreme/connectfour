{-# LANGUAGE NamedFieldPuns #-}

module UI.Console
  ( runGame
  , printBoard
  )
where

import           Board
import           Data.List                      ( intercalate )
import qualified Data.Vector                   as V
import           Control.Monad                  ( unless )

runGame :: IO ()
runGame = runGame' mkBoard

runGame' :: Board -> IO ()
runGame' board = do
  printBoard board
  unless (isGameFinished board) $ doNextMove board

doNextMove :: Board -> IO ()
doNextMove board = do
  col <- readLn
  putStrLn ""
  let newBoard = dropChip board (col - 1)
  maybe (putStrLn "Invalid move\n" >> runGame' board) runGame' newBoard

printBoard :: Board -> IO ()
printBoard = putStrLn . boardToString

boardToString :: Board -> String
boardToString board =
  let
    rowsStr     = V.toList $ rowToString <$> rows board
    rowsIndex   = (<> " ") . show <$> [(1 :: Int) ..]
    colsIndex   = unwords $ show <$> [1 .. (colsCount board)]
    rowsWithIdx = intercalate "\n" . reverse $ zipWith (<>) rowsIndex rowsStr
  in
    rowsWithIdx <> "\n  " <> colsIndex <> "\n\n" <> stateToString (state board)

rowToString :: Row -> String
rowToString Row { cells } = unwords . V.toList $ cellToString <$> cells

cellToString :: Cell -> String
cellToString Empty          = " "
cellToString (Filled color) = chipToString color

chipToString :: Chip -> String
chipToString Red    = "X"
chipToString Yellow = "O"

stateToString :: GameState -> String
stateToString (Win    chip) = chipToString chip <> " wins!"
stateToString (ToPlay chip) = chipToString chip <> " to play..."
stateToString Draw          = "It's a draw!"
