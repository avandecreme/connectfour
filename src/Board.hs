{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}

module Board
  ( Board
  , Row(..)
  , Chip(..)
  , Cell(..)
  , GameState(..)
  , mkBoard
  , dropChip
  , rows
  , state
  , rowsCount
  , colsCount
  , isGameFinished
  , getAllowedMoves
  )
where

import           Data.Vector.Instances          ( )
import           Data.Hashable                  ( Hashable )
import           Data.Vector                    ( (!)
                                                , (//)
                                                )
import qualified Data.Vector                   as V
import           Data.List                      ( find )
import           GHC.Generics                   ( Generic )

data Board = Board { rows         :: V.Vector Row
                   , state        :: GameState
                   , connectCount :: Int
                   , rowsCount    :: Int
                   , colsCount    :: Int
                   } deriving (Eq, Generic, Hashable)

newtype Row = Row { cells :: V.Vector Cell }
  deriving (Eq, Generic, Hashable)

data GameState = ToPlay Chip | Win Chip | Draw
  deriving (Eq, Show, Generic, Hashable)

data Cell = Empty | Filled Chip
  deriving (Eq, Show, Generic, Hashable)

data Chip = Red | Yellow
  deriving (Eq, Show, Generic, Hashable)

mkBoard :: Board
mkBoard =
  let rowsCount = 6
      colsCount = 7
      mkRow     = Row $ V.replicate colsCount Empty
  in  Board { rows         = V.replicate rowsCount mkRow
            , state        = ToPlay Red
            , connectCount = 4
            , rowsCount
            , colsCount
            }

isGameFinished :: Board -> Bool
isGameFinished Board { state } = case state of
  ToPlay _ -> False
  _        -> True

getCell :: Board -> Int -> Int -> Cell
getCell Board { rows } row col = let getRow = rows ! row in cells getRow ! col

setCell :: Board -> Int -> Int -> Chip -> Board
setCell board@Board { rows } row col chip =
  let rowToUpdate = rows ! row
      updateCell  = Row $ cells rowToUpdate // [(col, Filled chip)]
  in  board { rows = rows // [(row, updateCell)] }

getAllowedMoves :: Board -> [Int]
getAllowedMoves Board { rows, rowsCount, colsCount } =
  let topRow = rows ! (rowsCount - 1)
      isColFree col = cells topRow ! col == Empty
  in  filter isColFree [0 .. colsCount - 1]

dropChip :: Board -> Int -> Maybe Board
dropChip board@Board { rows, colsCount } col =
  let isRowFree r = cells r ! col == Empty
      row  = V.findIndex isRowFree rows
      chip = nextChip board
  in  if 0 <= col && col < colsCount
        then updateState <$> (setCell board <$> row <*> Just col <*> chip)
        else Nothing

nextChip :: Board -> Maybe Chip
nextChip Board { state } = case state of
  ToPlay chip -> Just chip
  _           -> Nothing

updateState :: Board -> Board
updateState board@Board { state, rows } =
  let winningChip = case findWinningCell board of
        Just (Filled chip) -> Just chip
        _                  -> Nothing
      isDraw = all isRowFull rows
      isRowFull Row { cells } = Empty `notElem` cells
      handleNoWinner = if isDraw then Draw else changePlayer
      changePlayer   = if state == ToPlay Red then ToPlay Yellow else ToPlay Red
      newState       = maybe handleNoWinner Win winningChip
  in  board { state = newState }

findWinningCell :: Board -> Maybe Cell
findWinningCell board@Board { rowsCount, colsCount } =
  let cells = (,) <$> [0 .. rowsCount - 1] <*> [0 .. colsCount - 1]
      isWinning (row, col) = any
        (\f -> f board row col)
        [connectHorizontally, connectVertically, connectUpward, connectDownward]
  in  uncurry (getCell board) <$> find isWinning cells

connectHorizontally :: Board -> Int -> Int -> Bool
connectHorizontally board@Board { colsCount, connectCount } row col
  | cell == Empty        = False
  | lastCol >= colsCount = False
  | otherwise            = all (cell ==) cellsToCheck
 where
  cell         = getCell board row col
  lastCol      = col + connectCount - 1
  cellsToCheck = getCell board row <$> [col + 1 .. lastCol]

connectVertically :: Board -> Int -> Int -> Bool
connectVertically board@Board { rowsCount, connectCount } row col
  | cell == Empty        = False
  | lastRow >= rowsCount = False
  | otherwise            = all (cell ==) cellsToCheck
 where
  cell         = getCell board row col
  lastRow      = row + connectCount - 1
  cellsToCheck = getCell board <$> [row + 1 .. lastRow] <*> pure col

connectUpward :: Board -> Int -> Int -> Bool
connectUpward board@Board { colsCount, rowsCount, connectCount } row col
  | cell == Empty        = False
  | lastRow >= rowsCount = False
  | lastCol >= colsCount = False
  | otherwise            = all (cell ==) cellsToCheck
 where
  cell    = getCell board row col
  lastRow = row + connectCount - 1
  lastCol = col + connectCount - 1
  cellsToCheck =
    uncurry (getCell board) <$> zip [row + 1 .. lastRow] [col + 1 .. lastCol]

connectDownward :: Board -> Int -> Int -> Bool
connectDownward board@Board { colsCount, connectCount } row col
  | cell == Empty        = False
  | lastRow < 0          = False
  | lastCol >= colsCount = False
  | otherwise            = all (cell ==) cellsToCheck
 where
  cell         = getCell board row col
  lastRow      = row - connectCount + 1
  lastCol      = col + connectCount - 1
  cellsToCheck = uncurry (getCell board)
    <$> zip [row - 1, row - 2 .. lastRow] [col + 1 .. lastCol]
