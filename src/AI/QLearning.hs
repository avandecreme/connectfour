{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NamedFieldPuns #-}

module AI.QLearning
  ( QState
  , mkQState
  , chooseMove
  , runLearning
  )
where

import           Control.Arrow                  ( (&&&) )
import           Control.Monad                  (when,  void )
import           UI.Console                     ( printBoard )
import           Data.Maybe                     ( fromJust )
import           Board
import           Data.HashMap.Strict            ( HashMap
                                                , lookupDefault
                                                , insert
                                                , empty
                                                )
import           System.Random                  ( StdGen
                                                , getStdGen
                                                , randomR
                                                )

gamesCount :: Int
gamesCount = 10

runLearning :: IO ()
runLearning = do
  gen <- getStdGen
  void $ runLearning' (mkQState gen) 0

runLearning' :: QState -> Int -> IO QState
runLearning' state count =
  let loop newState = runLearning' newState $ count + 1
  in  if count == gamesCount
        then pure state
        else putStrLn ("Game " <> show count) >> (loop =<< runGame state True)
        -- else loop =<< runGame state (count >= gamesCount - 1)

runGame :: QState -> Bool -> IO QState
runGame qstate doPrint = do
  let (board, newQstate) = runGame' mkBoard qstate
  when doPrint $ printBoard board >> putStrLn ""
  pure newQstate

runGame' :: Board -> QState -> (Board, QState)
runGame' board qstate = if isGameFinished board
  then (board, qstate)
  else uncurry runGame' $ doNextMove board qstate

doNextMove :: Board -> QState -> (Board, QState)
doNextMove board qstate =
  let (col, newQstate) = chooseMove qstate board
      newBoard         = fromJust $ dropChip board col
  in  (newBoard, newQstate)

data QState = QState { q :: HashMap (Board, Int) Double
                     , gen :: StdGen
                     }

mkQState :: StdGen -> QState
mkQState gen = QState { q = empty, .. }

learningRate :: Double
learningRate = 0.9

winRating :: Double
winRating = 1000

drawRating :: Double
drawRating = 0

randomMoveProbability :: Int
randomMoveProbability = 0

epsilon :: Double
epsilon = 1e-14

chooseMove :: QState -> Board -> (Int, QState)
chooseMove qState@QState {..} board =
  let
    allowedMoves         = getAllowedMoves board
    (r         , gen'  ) = randomR (1, 100) gen
    (chosenMove, newGen) = if r <= randomMoveProbability
      then pickRandomMove gen' allowedMoves
      else pickBestMove qState board gen' allowedMoves
    chosenMoveRating = getMoveRating qState board chosenMove
    -- We know that the chosenMove is a legal move, so we can do fromJust
    newBoard         = fromJust $ dropChip board chosenMove
    newQA = chosenMoveRating + learningRate * getStateRating qState newBoard
    newQ             = insert (board, chosenMove) newQA q
  in
    (chosenMove, QState { q = newQ, gen = newGen })


pickBestMove :: QState -> Board -> StdGen -> [Int] -> (Int, StdGen)
pickBestMove qState board gen allowedMoves =
  let movesAndRatings = (id &&& getMoveRating qState board) <$> allowedMoves
      bestRating      = maximum $ snd <$> movesAndRatings
      hasBestRating (_, rating) = bestRating - rating <= epsilon
      bestMoves         = fst <$> filter hasBestRating movesAndRatings
      (moveIdx, newGen) = randomR (0, length bestMoves - 1) gen
  in  (bestMoves !! moveIdx, newGen)

pickRandomMove :: StdGen -> [Int] -> (Int, StdGen)
pickRandomMove gen allowedMoves =
  let (moveIdx, newGen) = randomR (0, length allowedMoves - 1) gen
  in  (allowedMoves !! moveIdx, newGen)

getMoveRating :: QState -> Board -> Int -> Double
getMoveRating QState { q } board move = lookupDefault 0 (board, move) q

getStateRating :: QState -> Board -> Double
getStateRating qState board = case state board of
  Draw  -> drawRating
  Win _ -> winRating
  ToPlay _ ->
    let playerToPlayRating =
            maximum $ getMoveRating qState board <$> getAllowedMoves board
    in  -playerToPlayRating
